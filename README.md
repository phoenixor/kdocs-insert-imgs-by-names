# KdocsInsertImgsByNames

#### 介绍
金山表单收集文字和图片后，导出汇总表丢失图片的解决方案

#### 软件架构
纯Python实现


#### 安装教程

1.  下载zip压缩包到本地并解压出来
2.  右键项目文件夹，使用vscode打开
3.  python -m venv venv在项目根目录创建虚拟环境
4.  venv\Scripts\activate激活虚拟环境，pip install -r requirements.txt安装依赖包
5.  python main.py运行程序

#### 打包exe教程
1.  下载UPX压缩工具，https://upx.github.io，将压缩包中的upx.exe放在venv/Scripts下
2.  pyinstaller --onefile main.spec，打包的exe文件在dist文件夹下

#### 使用说明
见程序内的提示