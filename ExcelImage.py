from pathlib import Path

from openpyxl import Workbook, load_workbook
from openpyxl.drawing.image import Image
from openpyxl.drawing.spreadsheet_drawing import (
    AnchorMarker,
    OneCellAnchor,
    TwoCellAnchor,
)
from openpyxl.drawing.xdr import XDRPositiveSize2D
from openpyxl.utils import column_index_from_string
from openpyxl.utils.units import pixels_to_EMU as p2e
from openpyxl.utils.units import pixels_to_points, points_to_pixels
from PIL import JpegImagePlugin

# openpyxl保存save报错：KeyError: ‘.mpo‘
# 这个原因是因为保存图片的时候PIL库不发兼容各个图片文件后缀，需开头加两行忽略图片后缀就可以了
JpegImagePlugin._getmp = lambda x: None

desktop_path = Path.home() / "desktop"
offset = 2  # 图片在单元格中偏移量，即距离单元格边框的空白大小
modify_px = 12  # 宽度修正像素，由于列宽和像素无法精确换算，试出来这个修正量


class ExcelImage:
    """Excel图片操作类，提供两种插入图片方式：1、嵌入单元格，2、单元格中居中"""

    def __init__(self, excel_path=None):
        self.wb, self.ws = (
            self.load_excel(excel_path) if excel_path else self.create_excel()
        )

    def create_excel(self, sheet_title: str = ""):
        """新建工作薄，返回默认的sheet"""
        wb = Workbook()
        ws = wb.active
        if sheet_title:
            ws.title = sheet_title
        print("成功新建表格")
        return wb, ws

    def load_excel(self, excel_path, sheet_name: str = ""):
        """导入已存在的工作薄，指定表名返回worksheet"""
        wb = load_workbook(excel_path)
        print("成功导入表格：", excel_path)
        return wb, (wb.active if not sheet_name else wb[sheet_name])

    def embed_image(
        self,
        row: int,
        col_letter: str,
        image_url: str | Path,
        cell_w_h: tuple[int, int],
    ):
        """嵌入单元格的方式插入图片，改变单元格尺寸时图片会变形
        
        :param row:         行号，从1开始
        :param col_letter:  列号字母，从A开始
        :param image_url:   图片地址
        :param cell_w_h:   图片单元格宽（字符）高（磅）
        :return:
        """
        try:
            img = Image(image_url)
            # 这里列号和行号从1开始
            (
                self.ws.column_dimensions[col_letter].width,
                self.ws.row_dimensions[row].height,
            ) = cell_w_h
            # AnchorMarker(列号、列偏移、行号、行偏移)，列号与行号表示锚点所在的单元格坐标，从0开始
            # AnchorMarker(0, 0, 0, 0)表示锚点位于A1单元格的左上角，AnchorMarker(2, 0, 4, 0)表示锚点位于C5单元格的左上角
            # 偏移量为正表示锚点向右/向下偏移，为负则是向左/向上
            _from = AnchorMarker(
                column_index_from_string(col_letter) - 1,
                p2e(offset),
                row - 1,
                p2e(offset),
            )
            to = AnchorMarker(
                column_index_from_string(col_letter), -p2e(offset), row, -p2e(offset)
            )
            img.anchor = TwoCellAnchor("twoCell", _from, to)
            self.ws.add_image(img)
        except Exception as e:
            print("插入嵌入式图片失败！错误信息：", e)

    def insert_image_center(
        self,
        row: int,
        col_letter: str,
        image_url: str | Path,
        cell_w_h: tuple[int, int],
    ):
        """将图片以居中方式插入单元格，改变单元格尺寸时图片大小和位置不变，前提是单元格尺寸已经确定（比如已经填好其他单元格，最后再插入图片）

        :param row:         行号，从1开始
        :param col_letter:  图片列对应字母
        :param image_url:   图片路径
        :param cell_w_h:   图片单元格宽（字符）高（磅）
        :return: None
        """
        try:
            # 设置单元格列宽
            self.ws.column_dimensions[col_letter].width = cell_w_h[0]
            col_off = offset
            # 获取图片
            img = Image(image_url)
            # 计算原图片的宽高比例
            w_h_ratio = img.width / img.height
            # 根据单元格宽度设置图片宽度，像素≈列宽*8，无法精确计算，再减掉修正像素防止图片宽度超出单元格
            resize_img_width = cell_w_h[0] * 8 - col_off * 2 - modify_px
            resize_img_height = resize_img_width / w_h_ratio
            # 把原图片固定为实际的宽高
            img.width, img.height = resize_img_width, resize_img_height
            # ！！!这里有坑，当行高为默认值时（如新建表格，没有调整过行高），row_dimensions[row].height获取的值为None
            # 当行高为None或小于图片高度时，行高设置成图片高度
            cell_height = self.ws.row_dimensions[row].height
            if not cell_height or cell_height <= pixels_to_points(img.height):
                self.ws.row_dimensions[row].height = pixels_to_points(
                    img.height + offset * 2
                )
                row_off = offset
            else:
                # 当行高大于图片高度时，保持此时的行高让图片上下居中
                cell_height = self.ws.row_dimensions[row].height
                # 上下偏移量（相对于图片所在的单元格）
                row_off = int((points_to_pixels(cell_height) - img.height) / 2)

            # 计算出图片在excel中位置，是图片的大小
            # EMU（English Metric Unit）是一种长度单位，它是 Microsoft Office 中长度单位的基本单位，表示为二十英寸单位，即每英寸数以 914400 为分母还得数值。在openpyxl库中，工作表尺寸、行高、列宽等都是以EMU为单位进行设置。
            # XDRPositiveSize2D类是openpyxl库中提供的表示二维大小的类，它有两个属性，即cx（宽度值，以EMU为单位）和cy（高度值，以EMU为单位），也就是表示大小的二元组。在代码中，使用 XDRPositiveSize2D 来设置需要插入图片的大小，以将像素的图片大小信息转换为openpyxl能够识别的单位。
            # XDRPositiveSize2D(p2e(img.width), p2e(img.height))表示将图片的宽度和高度转换为单位 EMU，并用这两个值作为参数创建 XDRPositiveSize2D 对象，用于在Excel表格中设置图片大小。
            size = XDRPositiveSize2D(p2e(img.width), p2e(img.height))
            # 列号与行号表示锚点所在的单元格坐标，从0开始
            marker = AnchorMarker(
                col=column_index_from_string(col_letter) - 1,
                colOff=p2e(col_off),
                row=row - 1,
                rowOff=p2e(row_off),
            )
            img.anchor = OneCellAnchor(_from=marker, ext=size)
            self.ws.add_image(img)
        except Exception as e:
            print("居中插入图片失败！错误信息：", e)

    def save_excel(self, excel_path):
        self.wb.save(excel_path)
        print("【保存成功】", excel_path)


if __name__ == "__main__":
    excel_image = ExcelImage()
    for i in range(1, 9):
        excel_image.insert_image_center(i, "A", desktop_path / f"{i}.jpg")
    excel_image.save_excel(desktop_path / "证件照.xlsx")
