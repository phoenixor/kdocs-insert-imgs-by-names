from pathlib import Path

import pandas as pd
from openpyxl.utils import get_column_letter

from ExcelImage import ExcelImage


def insert_imgs_by_names(
    excel_path: str | Path,
    imgs_folder: str | Path,
    imgs_col_name: str,
    cell_w_h: tuple[int, int],
):
    """根据照片列的名称，逐个查找照片文件夹的照片，并从照片列的后一列开始横向逐个插入单元格"""
    df = pd.read_excel(excel_path, dtype=str)
    excel_image = ExcelImage(excel_path)
    for row_index, row in df.iterrows():
        image_names_list = row[imgs_col_name].split("\n")
        print(f"正在处理第 {row_index+1} 行，照片文件名：{image_names_list}")
        for i, image_name in enumerate(image_names_list, start=1):
            # 表格中的图片名时和分用:分隔，文件夹的图片名是用_分隔
            img_name = image_name.replace(":", "_")
            image_path = Path(imgs_folder).joinpath(img_name)
            if not image_path.exists():
                print(f"照片文件 {img_name} 不存在，请检查")
                continue
            col_index = df.columns.get_loc(imgs_col_name) + i
            col_letter = get_column_letter(col_index + 1)
            # pandas的行号从0开始，openpyxl的行号从1开始
            excel_image.insert_image_center(
                row_index + 1 + 1, col_letter, image_path, cell_w_h
            )

    excel_image.save_excel(
        Path(excel_path).with_name("（已插图片）" + Path(excel_path).name)
    )


if __name__ == "__main__":
    print(
        """
准备工作：
1、从金山表单的后台关联汇总表到多维表格，进入多维表格，在左边栏的表格视图，点击右侧 ... ，导出数据-压缩文件。
2、解压到电脑上，里面应该是一份xlsx表格和一个图片文件夹，检查表格中的图片列是否有“打包失败”的关键词，如果有，重新执行第一步。
3、如果图片都打包成功，继续执行本代码。
**************************************************************************
"""
    )
    excel_file_path = input("表格文件路径：").strip()
    image_folder_path = input("图片文件夹路径：").strip()
    imgs_col_name = input("照片列的列名：").strip()
    insert_imgs_by_names(excel_file_path, image_folder_path, imgs_col_name, (18, 55))
